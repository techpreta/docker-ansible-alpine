FROM alpine:3.14
RUN apk update && apk --no-cache add ansible py3-pip py3-wheel
RUN adduser -D ansible
USER ansible
CMD ["/bin/sh"]
